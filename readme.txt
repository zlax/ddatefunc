=== Discordian Date Function ===
Contributors: zlaxyi
Donate link: https://is3.soundragon.su/discordian-date-function
Tags: date, time, discordian, erisian, widget, eris, discordia, calendar, ddate, dtime
Requires at least: 2.0
Tested up to: 6.6
Stable tag: trunk
License: DWTWL 2.55
License URI: https://soundragon.su/license/license.html

This plugin provides Erisian (Discordian) dates and time in Wordpress.
Also provides shortcode and widget which displays the clock, current date according to the Discordian calendar, with notification of more than 70 holidays.

== Description ==

This plugin allows WordPress to easily show customizable Erisian dates and time (decimal) instead of the standard Gregorian dates. No theme changes are required.

The Discordian Date Function plugin provides a widget which will display the current Erisian time and date according to the [Discordian calendar](http://en.wikipedia.org/wiki/Discordian_calendar). This functional based on the Discordian Date plugin by Dan Johnson.

Also you can add the shortcode [today_ddate] in posts or pages for show the Discordian date of today.

== Installation ==

1. Extract the files from archive.
1. Upload the folder ddatefunc and its contents to the /wp-content/plugins/ directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.
1. Press 'Settings' for customize date string.
1. If you want to add widget: position the widget in an available area on your pages using the Widgets section of your WordPress administration control panel.
1. Use the widget options to give the widget any title you'd like.
1. (Optional) if you want to add the erisian date of today than you need to add <?php today_ddate() ?> where you want.

== Frequently Asked Questions ==

= Do you have any questions? =

Feel free to ask any questions about this plugin at the [Discordian Date Function plugin web page](https://is3.soundragon.su/discordian-date-function/).

== Screenshots ==

1. The screenshot shows the Discordian date text shown under post and comment.

== Changelog ==

= 2.5555 =
*Release Date - 17th day of Confusion 3190*

* Customisable seconds in the widget
* Specification of holidays
* Minor fixes

= 2.555 =
*Release Date - 52nd day of Confusion, 3188*

* Add optional Erisian time clock to the widget.

= 2.5 =
*Release Date - 21st day of Discord, 3186*

* Add optional [Erisian (decimal) time](https://www.reddit.com/r/Time/comments/fhys8v/erisian_time/) support.

= 0.555 =
*Release Date - 60th day of Chaos, 3186*

* Fixed Tib's day shift according Gregorian calendar's leap year.
* Add St. Mammes's day, fixed some other holiday strings.

= 0.55 =
*Release Date - 5th day of Discord, 3183*

* Fixed names of some Whollydays.
* Add settings page.
* Add customizable date string.

= 0.5 =
*Release Date - 2nd day of Discord, 3183*

* First public release.

== Upgrade Notice ==

= 2.5555 =
This version is actual.
