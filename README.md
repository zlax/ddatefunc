Discordian Date Function

under DWTWL 2.55 license: https://soundragon.su/license/license.html

https://wordpress.org/plugins/discordian-date-function/

This plugin allows WordPress to easily show Erisian dates and time (decimal) instead of the standard Gregorian dates. No theme changes are required.

This functional based on the Discordian Date plugin by Dan Johnson.

Also provides a widget which displays the current date according to the Discordian calendar, with notification of more than 70 holidays.

And you can add the shortcode [todаy_ddate] in posts or pages for show the Discordian date of today.
