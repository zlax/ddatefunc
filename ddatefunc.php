<?php
/*
Plugin Name: Discordian Date Function
Plugin URI: https://is3.soundragon.su/discordian-date-function
Description: Convert dates in Wordpress to customizable Discordian dates and time (decimal). Also, this plugin provides shortcode and widget used to display the current erisian date with notification of 70 holidays. Based on the Discordian Date plugin by Dan Johnson.
Version: 2.5555
Author: ivan zlax
Author URI: https://is3.soundragon.su/about
*/

function get_ddate($content, $format = "", $originalRequest = null) {

  /*
  Based on: http://www.dexterityunlimited.com/wordpress-plugins/discordian-date/ by Dan Johnson
  */

  $season_list = array("Chaos", "Discord", "Confusion", "Bureaucracy", "Aftermath");
  $day_list = array("Sweetmorn", "Boomtime", "Pungenday", "Prickle-Prickle", "Setting Orange");

  switch ($originalRequest) {
    case "get_the_date":
    case "get_the_time":
      $post = get_post( $format );
      if (get_option('dtime_convert'))
        $standard_date = utcm5_convert(mysql2date( 'U', $post->post_date_gmt ));
      else
        $standard_date = getdate (mysql2date( 'U', $post->post_date ));
      break;
    case "get_comment_date":
    case "get_comment_time":
      $comment = get_comment( $format );
      if (get_option('dtime_convert'))
        $standard_date = utcm5_convert(mysql2date( 'U', $comment->comment_date_gmt ));
      else
        $standard_date = getdate (mysql2date( 'U', $comment->comment_date ));
      break;
    case "now":
      if (get_option('dtime_convert'))
        $standard_date = utcm5_convert(getdate()[0]);
      else
        $standard_date = getdate();
  }

  if ($originalRequest == "get_comment_time") return dtime_convert($standard_date);
  if ($originalRequest == "get_the_time") return dtime_convert($standard_date);

  $dyear=$standard_date["year"]+1166;
  $gyear=$standard_date["year"];
  $yday=$standard_date["yday"];
  // Check Tib's day according to Gregorian calendar
  if ($gyear%4==0&&$gyear%100!=0 || $gyear%400==0) {
    if ($standard_date["yday"]==59) $sttib="St. Tib's";
    if ($standard_date["yday"]>59) $yday=$standard_date["yday"]-1;
  }
  $mon=$standard_date["mon"];
  $mday=$standard_date["mday"];
  $dseason=(int)($yday/73);
  $name_season=$season_list[$dseason];
  $dday=($yday-(73*$dseason))+1;
  $clear_dday=$dday;
  $suff=$dday%10;

  switch ($suff) {
    case 1:
      $dday.="st";
      break;
    case 2:
      $dday.="nd";
      break;
    case 3:
      $dday.="rd";
      break;
    default:
      $dday.="th";
  }

  $dweekday=$day_list[($yday%5)];

  if ($originalRequest == "now") {
    $ddate=$dweekday.", the ".$dday." day of ".$name_season.", in the yold ".$dyear;
    if (isset($sttib)) $ddate=$sttib." day, in the yold ".$dyear;
  } else {
    $patterns = array();
    $patterns[0] = '%DY';
    $patterns[1] = '%DS';
    $patterns[2] = '%DD';
    $patterns[3] = '%DC';
    $patterns[4] = '%DW';
    $patterns[5] = '%GY';
    $patterns[6] = '%GM';
    $patterns[7] = '%GN';
    $patterns[8] = '%GD';
    $patterns[9] = '%GW';
    $patterns[10] = '%DT';
    $patterns[11] = '%CT';
    $patterns[12] = '%UT';
    $replacements = array();
    $replacements[0] = $dyear;
    $replacements[1] = $name_season;
    $replacements[2] = $dday;
    $replacements[3] = $clear_dday;
    if (isset($sttib)) {
      $replacements[2] = $sttib;
      $replacements[3] = "tib";
    }
    $replacements[4] = $dweekday;
    $replacements[5] = $standard_date["year"];
    $replacements[6] = $standard_date["month"];
    $replacements[7] = $standard_date["mon"];
    $replacements[8] = $standard_date["mday"];
    $replacements[9] = $standard_date["weekday"];
    $replacements[10] = dtime_convert($standard_date);
    $replacements[11] = $standard_date["hours"] . ":" . str_pad($standard_date["minutes"], 2, '0', STR_PAD_LEFT);
    $replacements[12] = $standard_date[0];
    $ddate = str_replace($patterns, $replacements, get_option('ddatefunc_string'));
  }
  return $ddate;
}

function dtime_convert($standard_date) {
    $standard_msday = $standard_date["hours"]*3600000+$standard_date["minutes"]*60000+$standard_date["seconds"]*1000;
    $discordian_sday = round($standard_msday/864);
    $discordian_minutes = $discordian_sday%10000;
    $dtime = floor($discordian_sday/10000) . ":" . str_pad(floor($discordian_minutes/100), 2, '0', STR_PAD_LEFT);
    return $dtime;
}

function utcm5_convert($standard_date) {
    // Easter Island Winter Time Zone offset (-5*60*60)
    $utcm5_date = array();
    $utcm5_date[0] = $standard_date;
    $unixtime_utcm5 = $standard_date-18000;
    $utcm5_date["seconds"] = gmdate("s", $unixtime_utcm5);
    $utcm5_date["minutes"] = gmdate("i", $unixtime_utcm5);
    $utcm5_date["hours"] = gmdate("G", $unixtime_utcm5);
    $utcm5_date["mday"] = gmdate("j", $unixtime_utcm5);
    $utcm5_date["wday"] = gmdate("w", $unixtime_utcm5);
    $utcm5_date["mon"] = gmdate("n", $unixtime_utcm5);
    $utcm5_date["year"] = gmdate("Y", $unixtime_utcm5);
    $utcm5_date["yday"] = gmdate("z", $unixtime_utcm5);
    $utcm5_date["weekday"] = gmdate("l", $unixtime_utcm5);
    $utcm5_date["month"] = gmdate("F", $unixtime_utcm5);
    return $utcm5_date;
}

class Discordian_Date_Widget extends WP_Widget {

  function Discordian_Date_Widget() {
    $widget_ops = array('classname' => 'discordian_date_func', 'description' => 'Display the current date according to the Discordian Calendar.');
    $control_ops = array('width' => 200, 'height' => 120);
    $this->WP_Widget('discordian_date_func', 'Discordian Date Widget', $widget_ops, $control_ops);
  }

  function widget($args, $instance) {

    $now = getdate();
    $yday=$now["yday"];
    $mon=$now["mon"];
    $mday=$now["mday"];
    $dseason=(int)($yday/73);
    if ($now["year"]%4==0&&$now["year"]%100!=0 || $now["year"]%400==0)            
      if ($now["yday"]>59) $yday=$now["yday"]-1;
    $dday=($yday-(73*$dseason))+1;
    $a_holiday=array("Mungday", "Mojoday", "Syaday", "Zaraday", "Maladay");
    $s_holiday=array("Chaoflux", "Discoflux", "Confuflux", "Bureflux", "Afflux");
    $m_holiday=array("Chaomas", "Discomas", "Confumas", "Buremas", "Afmas");
    $t_holiday=array("Chaosloth", "Discosloth", "Confusloth", "Buresloth", "Afsloth");
    $e_holiday=array("Mungeye", "Mojeye", "Syadeye", "Zareye", "Maleye");
    $holiday="";

    if ($dday==5) {
      $holiday = " Celebrate Apostle Day, ".$a_holiday[$dseason].".";
    } elseif ($dday==23) {
      $holiday = " Celebrate Synaptyclypse Day, ".$m_holiday[$dseason].".";
    } elseif ($dday==27) {
      $holiday = " Celebrate Sloth Day, ".$t_holiday[$dseason].".";
    } elseif ($dday==50) {
      $holiday = " Celebrate Flux Day, ".$s_holiday[$dseason].".";
    } elseif ($dday==73) {
      $holiday = " Celebrate Eye Day, ".$e_holiday[$dseason].".";
    } elseif ($mon==2 && $mday==29) {
      $holiday = " Celebrate St. Tib's Day.";
    } elseif ($dseason==0 && $dday==8) {
      $holiday = " Celebrate Death of Emperor Norton.";
    } elseif ($dseason==0 && $dday==10) {
      $holiday = " Celebrate Backwards/Binary Day.";
    } elseif ($dseason==0 && $dday==11) {
      $holiday = " Celebrate RAW Day.";
    } elseif ($dseason==0 && $dday==14) {
      $holiday = " Celebrate Golden Apple Presentation Day.";
    } elseif ($dseason==0 && $dday==17) {
      $holiday = " Celebrate Joshmas.";
    } elseif ($dseason==0 && $dday==18) {
      $holiday = " Celebrate Pat Pineapple Day.";
    } elseif ($dseason==0 && $dday==21) {
      $holiday = " Celebrate Hug Day.";
    } elseif ($dseason==0 && $dday==26) {
      $holiday = " Celebrate yaD sdrawkcaB, Traditional.";
    } elseif ($dseason==0 && $dday==37) {
      $holiday = " Celebrate Aaron Burr's Birthday.";
    } elseif ($dseason==0 && $dday==51) {
      $holiday = " Celebrate Pet Loving Day.";
    } elseif ($dseason==0 && $dday==69) {
      $holiday = " Celebrate Chicken Head Day.";
    } elseif ($dseason==0 && $dday==72) {
      $holiday = " Celebrate Daytime.";
    } elseif ($dseason==1 && $dday==4) {
      $holiday = " Celebrate Grover Cleveland's Birthday.";
    } elseif ($dseason==1 && $dday==11) {
      $holiday = " Celebrate Discordians for Jesus/Love Your Neighbor Day.";
    } elseif ($dseason==1 && $dday==18) {
      $holiday = " Celebrate Fool's Day.";
    } elseif ($dseason==1 && $dday==19) {
      $holiday = " Celebrate St. John the Blasphemist's Day.";
    } elseif ($dseason==1 && $dday==34) {
      $holiday = " Celebrate Omarmas.";
    } elseif ($dseason==1 && $dday==43) {
      $holiday = " Celebrate Universal Ordination Day.";
    } elseif ($dseason==1 && $dday==68) {
      $holiday = " Celebrate Mal-2mas.";
    } elseif ($dseason==1 && $dday==70) {
      $holiday = " Celebrate Day of the Elppin/Defenestration of Prague Day.";
    } elseif ($dseason==1 && $dday==72) {
      $holiday = " Celebrate Towel Day.";
    } elseif ($dseason==2 && $dday==26) {
      $holiday = " Celebrate Imaginary Friend.";
    } elseif ($dseason==2 && $dday==28) {
      $holiday = " Celebrate St. George's Day.";
    } elseif ($dseason==2 && $dday==30) {
      $holiday = " Celebrate Zoog Day.";
    } elseif ($dseason==2 && $dday==37) {
      $holiday = " Celebrate Mid-Year's Day.";
    } elseif ($dseason==2 && $dday==40) {
      $holiday = " Celebrate X-Day.";
    } elseif ($dseason==2 && $dday==55) {
      $holiday = " Celebrate Mal-2 Day.";
    } elseif ($dseason==2 && $dday==57) {
      $holiday = " Celebrate John Dillinger Day.";
    } elseif ($dseason==3 && $dday==3) {
      $holiday = " Celebrate Multiversal Underwear Day.";
    } elseif ($dseason==3 && $dday==10) {
      $holiday = " Celebrate St. Cecil Day.";
    } elseif ($dseason==3 && $dday==18) {
      $holiday = " Celebrate Spanking Fest.";
    } elseif ($dseason==3 && $dday==33) {
      $holiday = " Celebrate Pussyfoot Day.";
    } elseif ($dseason==3 && $dday==37) {
      $holiday = " Celebrate Mass of Planet Eris/Mass of Eristotle.";
    } elseif ($dseason==3 && $dday==39) {
      $holiday = " Celebrate St. Mammes's Day.";
    } elseif ($dseason==3 && $dday==41) {
      $holiday = " Celebrate Emperor Norton Proclamation Day.";
    } elseif ($dseason==3 && $dday==55) {
      $holiday = " Celebrate Feast of St. John Blasphemist.";
    } elseif ($dseason==3 && $dday==57) {
      $holiday = " Celebrate Shamlicht Kids Club Day.";
    } elseif ($dseason==3 && $dday==59) {
      $holiday = " Celebrate Gonculator Day.";
    } elseif ($dseason==3 && $dday==60) {
      $holiday = " Celebrate Mad Hatter Day.";
    } elseif ($dseason==3 && $dday==66) {
      $holiday = " Celebrate Habeas Corpus Remembrance Day.";
    } elseif ($dseason==4 && $dday==17) {
      $holiday = " Celebrate Pope Night.";
    } elseif ($dseason==4 && $dday==28) {
      $holiday = " Celebrate Ek-sen-triks CluborGuild Day.";
    } elseif ($dseason==4 && $dday==37) {
      $holiday = " Celebrate 537 Day.";
    } elseif ($dseason==4 && $dday==40) {
      $holiday = " Celebrate Omar's Day.";
    } elseif ($dseason==4 && $dday==43) {
      $holiday = " Celebrate Day D.";
    } elseif ($dseason==4 && $dday==46) {
      $holiday = " Celebrate Hug Day II.";
    } elseif ($dseason==4 && $dday==56) {
      $holiday = " Celebrate Agnostica.";
    } elseif ($dseason==4 && $dday==65) {
      $holiday = " Celebrate Circlemas.";
    }

    extract($args);
    $title = $instance['title'];
    $body = apply_filters('widget_text', $instance['body']);

    ob_start();
    echo "<div class=\"discordian_date_func\" style=\"width:100%;text-align:center;font-size:18px;line-height:36px;padding-top:40px;padding-bottom:40px;\">";
    if (get_option('dtime_widget')) {
        if (get_option('dtime_convert'))
            echo "<script type=\"text/javascript\">var eastertime = true;</script>";
        if (get_option('dtime_widgetsec'))
            echo "<script type=\"text/javascript\">var widgetsec = true;</script>";
        echo "<h1 style=\"font-size: 255%\" id=\"dtime\"></h1>";
    }
    echo "Today is ";
    echo get_ddate($content, $format, "now").".";
    if ($holiday) echo "<br><b>".$holiday."</b>";
    echo "</div>";
    $body2 = ob_get_contents();
    ob_end_clean();

    if (!empty($title2)||!empty($body2)) echo $before_widget;
    if (!empty($title2)) echo $before_title,$title2,$after_title;
    if (!empty($body2)) echo $body2;
    if (!empty($title2)||!empty($body2)) echo $after_widget;
  }

  function update($new_instance, $old_instance) {
    return $new_instance;
  }

  function form($instance) {
    $title = $instance['title'];
    $title_id = $this->get_field_id('title');
    $title_name = $this->get_field_name('title');
?>
        <p>
            <label for="<?php echo $title_id; ?>"><strong>Title:</strong></label>
            <input class="widefat" id="<?php echo $title_id; ?>" name="<?php echo $title_name; ?>"
                   type="text" value="<?php echo esc_attr($title); ?>"/>
        </p>
<?php
  }
}

function get_the_ddate_post($content, $format = null) {
  return get_ddate($content, $format, "get_the_date"); }

function get_the_dtime_post($content, $format = null) {
  return get_ddate($content, $format, "get_the_time"); }

function get_the_ddate_comment($content, $format = null) {
  return get_ddate($content, $format, "get_comment_date"); }

function get_the_dtime_comment($content, $format = null) {
  return get_ddate($content, $format, "get_comment_time"); }

function register_ddatefunc_widget() {
  register_widget( 'Discordian_Date_Widget' );
}

function today_ddate() {
  echo return_today_hebDate();
}

function return_today_ddate() {
  return get_ddate($content, $format, "now");
}

$settings_name = "Discordian Date Function Settings";

function ddatefunc_add_admin() {
  global $settings_name;
  add_options_page(__('Settings').': '.$settings_name, $settings_name, 'edit_themes', basename(__FILE__), 'ddatefunc_admin');
}

function ddatefunc_admin() {
  global $settings_name;
?>
<div class="wrap">
<?php
  screen_icon();
  echo '<h2>'.$settings_name.'</h2>';
  if (isset($_POST['save'])) {
    update_option('ddatefunc_string', stripslashes($_POST['textfield']));
    if (isset($_POST['ddatefunc_change_checkbox']))
      update_option('ddatefunc_change', "1");
    else
      update_option('ddatefunc_change', "0");
    if (isset($_POST['dtimefunc_change_checkbox']))
      update_option('dtimefunc_change', "1");
    else
      update_option('dtimefunc_change', "0");
    if (isset($_POST['dtime_convert_checkbox']))
      update_option('dtime_convert', "1");
    else
      update_option('dtime_convert', "0");
    if (isset($_POST['dtime_widget_checkbox']))
      update_option('dtime_widget', "1");
    else
      update_option('dtime_widget', "0");
    if (isset($_POST['dtime_widgetsec_checkbox']))
      update_option('dtime_widgetsec', "1");
    else
      update_option('dtime_widgetsec', "0");
    echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><b>'.__('Settings saved.').'</b></p></div>';
  }
?>
  <form method="post">
    <table class="form-table">
      <tr valign="top">
        <th scope="row">Date string:</th>
        <td>
          <input name="textfield" class="regular-text" type="text" value="<?php echo get_option('ddatefunc_string'); ?>" >
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Legend:</th>
        <td>
<b>%DY</b> - Discordian yold (example: 3180)<br>
<b>%DS</b> - Erisian season name (example: Chaos)<br>
<b>%DD</b> - Discordian season day (example: 5th)<br>
<b>%DC</b> - Clear erisian day number (example: 5)<br>
<b>%DW</b> - Discordian week day (example: Setting Orange)<br>
<b>%GY</b> - Gregorian year (example: 2014)<br>
<b>%GM</b> - Gregorian month (example: January)<br>
<b>%GN</b> - Gregorian month number (example: 1)<br>
<b>%GD</b> - Gregorian month day (example: 5)<br>
<b>%GW</b> - Gregorian weekday (example: Friday)<br>
<b>%DT</b> - Discordian (decimal) time (example: 5:67)<br>
<b>%CT</b> - Christian time (example: 23:00)<br>
<b>%UT</b> - Unix time (example: 1555222555)<br>
<br>
Examples<br>
<br>
<b>%DW, the %DD day of %DS, in the yold %DY</b><br> returns:<br><b>Setting Orange, the 5th day of Chaos, in the yold 3180</b><br>
<br>
<b>%DC %DS, %DY (%GN.%GN.%GY)</b><br> returns:<br><b>5 Chaos, 3180 (5.1.2014)</br>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Convert all post and comment dates:</th>
        <td>
          <fieldset>
          <legend class="screen-reader-text">
          <span>Convert to Discordian dates</span>
          </legend>
          <label for="users_can_register">
          <input name="ddatefunc_change_checkbox" id="users_can_register" type="checkbox" value="1" <?php if(get_option('ddatefunc_change')==1) { echo 'checked="checked"'; } ?>>
          </label>
          </fieldset>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Convert Christian time to Discordian (decimal) time:</th>
        <td>
          <fieldset>
          <legend class="screen-reader-text">
          <span>Convert to Erisian time</span>
          </legend>
          <label for="users_can_register">
          <input name="dtimefunc_change_checkbox" id="users_can_register" type="checkbox" value="1" <?php if(get_option('dtimefunc_change')==1) { echo 'checked="checked"'; } ?>>
          </label>
          </fieldset>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Use Erisian start of the day - <a href="https://discordia.fandom.com/wiki/Erisian_Time">starts 5 Christian hours later than the day in London</a>:</th>
        <td>
          <fieldset>
          <legend class="screen-reader-text">
          <span>Erisian start of the day</span>
          </legend>
          <label for="users_can_register">
          <input name="dtime_convert_checkbox" id="users_can_register" type="checkbox" value="1" <?php if(get_option('dtime_convert')==1) { echo 'checked="checked"'; } ?>>
          </label>
          </fieldset>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Show Erisian time clock in the widget:</th>
        <td>
          <fieldset>
          <legend class="screen-reader-text">
          <span>Erisian time in the widget</span>
          </legend>
          <label for="users_can_register">
          <input name="dtime_widget_checkbox" id="users_can_register" type="checkbox" value="1" <?php if(get_option('dtime_widget')==1) { echo 'checked="checked"'; } ?>>
          </label>
          </fieldset>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Display seconds in erisian time clock in the widget:</th>
        <td>
          <fieldset>
          <legend class="screen-reader-text">
          <span>Seconds in the widget</span>
          </legend>
          <label for="users_can_register">
          <input name="dtime_widgetsec_checkbox" id="users_can_register" type="checkbox" value="1" <?php if(get_option('dtime_widgetsec')==1) { echo 'checked="checked"'; } ?>>
          </label>
          </fieldset>
        </td>
      </tr>
    </table>
  <div class="submit">
    <input name="save" type="submit" class="button-primary" value="<?php echo __('Save Cganges'); ?>" />
  </div>
  </form>
</div>
<?php
}

function ddatefunc_settings($actions, $plugin_file) {
  if (false === strpos($plugin_file, basename(__FILE__)))
    return $actions;

  $settings_link = '<a href="options-general.php?page=ddatefunc.php' .'">Settings</a>';
  array_unshift($actions, $settings_link);
  return $actions;
}

function ddatefunc_deinstall() {
  delete_option('ddatefunc_string');
  delete_option('ddatefunc_change');
  delete_option('dtimefunc_change');
  delete_option('dtime_convert');
  delete_option('dtime_widget');
  delete_option('dtime_widgetsec');
}

function ddatefunc_init() {
    wp_enqueue_script('dtime', plugins_url('/dtime.js', __FILE__));
}

add_shortcode('today_ddate', 'return_today_ddate');
add_action('widgets_init', 'register_ddatefunc_widget');
add_option('ddatefunc_string', '%DD day of %DS, in the yold %DY');
add_option('ddatefunc_change', '1');
add_option('dtimefunc_change', '1');
add_option('dtime_convert', '1');
add_option('dtime_widget', '1');
add_option('dtime_widgetsec', '1');

add_filter('plugin_action_links', 'ddatefunc_settings', 10, 2);
add_action('admin_menu', 'ddatefunc_add_admin');

if (get_option('ddatefunc_change')) {
  add_filter('get_the_date','get_the_ddate_post');
  add_filter('get_comment_date','get_the_ddate_comment');
}

if (get_option('dtimefunc_change')) {
  add_filter('get_the_time','get_the_dtime_post');
  add_filter('get_comment_time','get_the_dtime_comment');
}

if (get_option('dtime_widget'))
  add_action('wp_enqueue_scripts','ddatefunc_init');

if (function_exists('register_uninstall_hook'))
  register_uninstall_hook(__FILE__, 'ddatefunc_deinstall');
