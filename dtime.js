/*
* based on:
* https://gitlab.com/zlax/dtime-js
*/

function addZero(numeral) {
    if (numeral < 10)
        return "0"+numeral;
    return numeral;
}

function dtime(gregorianDate,resultar="default") {
    if (typeof eastertime != "undefined") {
        // Easter Island Winter Time Zone offset (-5*60*60*1000)
        var date = new Date(gregorianDate.getTime()-18000000);
        var christianMSDay = date.valueOf() % 86400000;
    } else {
        // Christian Time Zone calculation
        var date = new Date(gregorianDate.getTime());
        var h = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();
        var ms = date.getMilliseconds();
        var christianMSDay = h*3600000+m*60000+s*1000+ms;
    }
    var erisianSecondsDay = (christianMSDay/864);
    var hour = Math.floor(erisianSecondsDay/10000);
    erisianSecondsDay %= 10000;
    var minute = Math.floor(erisianSecondsDay/100);
    erisianSecondsDay %= 100;
    var second = Math.floor(erisianSecondsDay);
    
    switch (resultar) {
        case "seconds":
            return hour+":"+addZero(minute)+":"+addZero(second);
            break;
        default:
            return hour+":"+addZero(minute);
            break;
    }
}

function go() {
    if (typeof widgetsec != "undefined")
        document.getElementById("dtime").innerHTML = dtime(new Date(), "seconds");
    else
        document.getElementById("dtime").innerHTML = dtime(new Date());
}

window.onload = function() {
    go();
    // 1 discordian decimal second = 864 christian milliseconds
    setInterval(go, 864);
}
